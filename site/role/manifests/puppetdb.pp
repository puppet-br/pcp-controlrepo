class role::puppetdb {
  include profile::ntp
  include profile::puppet::agent
  include profile::mcollective::server
  include profile::puppetdb::database
  include profile::puppetdb::app
  include profile::puppetdb::frontend
  
  Class['profile::ntp'] ->
  Class['profile::puppet::agent'] ->
  Class['profile::mcollective::server'] ->
  Class['profile::puppetdb::database'] ->
  Class['profile::puppetdb::app'] ->
  Class['profile::puppetdb::frontend']
}